package com.gitlab.archetypeslab.quarkuspoiconcept.business.configs;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import com.gitlab.archetypeslab.quarkuspoiconcept.business.services.ExcelProcessService;
import com.gitlab.archetypeslab.quarkuspoiconcept.framework.poi.DelegateWorkbookFactory;
import com.gitlab.archetypeslab.quarkuspoiconcept.framework.poi.WorkbookFactory;

@ApplicationScoped
public class ApplicationConfig {

    @Produces
    @ApplicationScoped
    public WorkbookFactory delegateWorkbookFactory() {
        return new DelegateWorkbookFactory();
    }

    @Produces
    @ApplicationScoped
    public ExcelProcessService excelProcessService(WorkbookFactory workbookFactory) {
        return new ExcelProcessService(workbookFactory);
    }

}
