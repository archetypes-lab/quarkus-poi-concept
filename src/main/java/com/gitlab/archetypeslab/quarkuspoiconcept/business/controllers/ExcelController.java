package com.gitlab.archetypeslab.quarkuspoiconcept.business.controllers;

import java.io.File;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

import com.gitlab.archetypeslab.quarkuspoiconcept.business.services.ExcelProcessService;
import com.gitlab.archetypeslab.quarkuspoiconcept.framework.files.FileUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/excel")
public class ExcelController {

    private static final Logger log = LoggerFactory.getLogger(ExcelController.class);

    private final ExcelProcessService excelProcessService;

    public ExcelController(ExcelProcessService excelProcessService) {
        this.excelProcessService = excelProcessService;
    }

    @Path("process-xls")
    @GET
    public void processXls() {
        log.debug("-> processXls");
        processExcelClasspath("/file_example_XLS_5000.xls");
    }

    @Path("process-xlsx")
    @GET
    public void processXlsx() {
        log.debug("-> processXlsx");
        processExcelClasspath("/file_example_XLSX_5000.xlsx");
    }

    // private

    private void processExcelClasspath(String classpathFile) {
        File tempFile = null;
        try {
            tempFile = FileUtils.copyFileFromClasspath(classpathFile);
            this.excelProcessService.readExcel(tempFile);
        } finally {
            FileUtils.deleteFile(tempFile);
        }
    }

}
