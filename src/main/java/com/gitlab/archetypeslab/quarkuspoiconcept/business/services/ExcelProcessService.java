package com.gitlab.archetypeslab.quarkuspoiconcept.business.services;

import java.io.File;
import java.io.IOException;

import com.gitlab.archetypeslab.quarkuspoiconcept.framework.poi.WorkbookFactory;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExcelProcessService {

    private static final Logger log = LoggerFactory.getLogger(ExcelProcessService.class);

    private WorkbookFactory workbookFactory;

    public ExcelProcessService(WorkbookFactory workbookFactory) {
        this.workbookFactory = workbookFactory;
    }

    public void readExcel(File fileExcel) {
        try (var workbook = workbookFactory.create(fileExcel)) {
            var sheet = workbook.getSheetAt(0);
            readSheet(sheet);
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

    // private

    private void readSheet(Sheet sheet) {
        var time1Millis = System.currentTimeMillis();
        var rowIterator = sheet.rowIterator();
        var rowColCount = new int[] { 0, 0 };
        while (rowIterator.hasNext()) {
            var row = rowIterator.next();
            readRow(row, rowColCount);
        }
        var time2Millis = System.currentTimeMillis();

        log.info("Total rows processed: {}, total cells processed: {}", rowColCount[0], rowColCount[1]);

        log.info("Total time: {} seconds", (time2Millis - time1Millis) / 1000);
    }

    private void readRow(Row row, int[] rowColCount) {
        var cellIterator = row.cellIterator();
        while (cellIterator.hasNext()) {
            var cell = cellIterator.next();

            var value = getCellValue(cell);

            log.info("row number: {}, cell number: {}, cell type: {}, cell value: {}", row.getRowNum(),
                    cell.getColumnIndex(), cell.getCellType(), value);
            rowColCount[1]++;
        }
        rowColCount[0]++;
    }

    private String getCellValue(Cell cell) {
        String value = null;
        switch (cell.getCellType()) {
            case BOOLEAN:
                value = String.valueOf(cell.getBooleanCellValue());
                break;
            case NUMERIC:
                value = String.valueOf(cell.getNumericCellValue());
                break;
            case STRING:
            default:
                value = cell.getStringCellValue();
        }
        return value;
    }

}
