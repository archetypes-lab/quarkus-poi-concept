package com.gitlab.archetypeslab.quarkuspoiconcept.framework.poi;

import java.io.File;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Workbook;

public class DelegateWorkbookFactory implements WorkbookFactory {

    @Override
    public Workbook create(File fileExcel) {
        try {
            return org.apache.poi.ss.usermodel.WorkbookFactory.create(fileExcel);
        } catch (EncryptedDocumentException | IOException e) {
            throw new IllegalStateException(e);
        }
    }

}
