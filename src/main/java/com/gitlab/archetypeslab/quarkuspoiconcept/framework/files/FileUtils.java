package com.gitlab.archetypeslab.quarkuspoiconcept.framework.files;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;
import java.util.UUID;

import org.apache.poi.util.IOUtils;

public final class FileUtils {

    private FileUtils() {
        // empty
    }

    public static File copyFileFromClasspath(String classpathFile) {

        File fileTemp;
        try (var inputStream = FileUtils.class.getResourceAsStream(classpathFile)) {

            if (inputStream == null) {
                throw new IllegalStateException(
                    String.format("No se encontro el recurso en classpath: %s", classpathFile)
                );
            }

            var optionalExtension = getExtensionByStringHandling(classpathFile);

            if (!optionalExtension.isPresent()) {
                throw new IllegalArgumentException(String.format("Not valid filename: %s", classpathFile));
            }

            var suffix = optionalExtension.get();

            fileTemp = Files.createTempFile(UUID.randomUUID().toString(), suffix).toFile();

            try (var outputStream = new FileOutputStream(fileTemp)) {
                IOUtils.copy(inputStream, outputStream);
            }

            return fileTemp;

        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }

    }

    public static Optional<String> getExtensionByStringHandling(String filename) {
        return Optional.ofNullable(filename).filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }

    public static void deleteFile(File file) {
        if (file == null) {
            return;
        }
        try {
            Files.delete(file.toPath());
        } catch (IOException ex) {
            throw new IllegalStateException(ex);
        }
    }

}
