package com.gitlab.archetypeslab.quarkuspoiconcept.framework.poi;

import java.io.File;

import org.apache.poi.ss.usermodel.Workbook;

public interface WorkbookFactory {
    
    Workbook create(File file);

}
